long long rosen(int x, int y)
{
	return ((1 - x) * (1 - x)) + (100 * (y - x*x) * (y - x*x));
}

long long sphere(int x, int y)
{
	return (long long) x*x + (long long) y*y;
}

long long booths(int x, int y)
{
	return (long long) pow((x + 2 * y - 7), 2) + (long long) pow((2 * x + y - 5), 2);
}

long long evalFunc(int x, int y)
{
	return sphere(x, y);
}