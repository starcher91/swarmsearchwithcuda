// 
//	vecadd_kernel.cu
//

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <curand_kernel.h>

#include <stdio.h>
#include <time.h>	// for clock_t

#define blockSize  (512)	// number of threads per block
#define MAX_NUM_BLOCKS 65535 // Max number of blocks

// Compute one vertor sum per thread
__global__ void vectorAddKernel(float *c, float *a, float *b, unsigned int N)
{
    int i = threadIdx.x + blockIdx.x * blockDim.x;
	if (i < N)
		c[i] = a[i] + b[i];
}

__global__ void vectorMultiAddKernal(float *c, float *a, float *b, unsigned int N, unsigned int numAdds, unsigned int numBlocks)
{
	int i;
	for (int j = 0; j < numAdds; j++)
	{
		i = (threadIdx.x + blockIdx.x * blockDim.x) + (j * numBlocks);
		if (i < N)
			c[i] = a[i] + b[i];
	}
}

__global__ void swarmSearchKernal(long long *result, int *x, int *y, int arraySize, int numMoves, int maxDistPerMove, int mapSize, curandState *states)
{
	int i = threadIdx.x + blockIdx.x * blockDim.x;
	int newX, newY, xDir, yDir, xOffset, yOffset;
	long long minResult;
	int currDistPerMove = maxDistPerMove;
	curand_init(1234, i, 0, &states[i]);
	if (i < arraySize)
	{
		for (int j = 0; j < numMoves; j++)
		{
			if (curand_uniform(&states[i]) < 0.5f)
				xDir = -1;
			else
				xDir = 1;

			if (curand_uniform(&states[i]) < 0.5f)
				yDir = -1;
			else
				yDir = 1;

			xOffset = xDir * (int) (curand_uniform(&states[i]) * currDistPerMove);
			yOffset = yDir * (int)(curand_uniform(&states[i]) * currDistPerMove);

			if (x[i] + xOffset < 0)
			{
				newX = mapSize + (x[i] + xOffset);
			}
			else
			{
				newX = (x[i] + xOffset) % mapSize;
			}

			if (y[i] + yOffset < 0)
			{
				newY = mapSize + (y[i] + yOffset);
			}
			else
			{
				newY = (y[i] + yOffset) % mapSize;
			}

			////booths function
			//minResult = (long long)((newX + 2 * newY - 7) * (newX + 2 * newY - 7)) + (long long) ((2 * newX + newY - 5) * (2 * newX + newY - 5));
			//sphere function
			minResult = (long long) (newX*newX) + (long long) (newY*newY);
			if (minResult < result[i])
			{
				x[i] = newX;
				y[i] = newY;
				result[i] = minResult;
				if(currDistPerMove > 0) currDistPerMove--;
			}
		}
	}
}

//////////////////////// Entry point for Cuda functionality on host side ////////////////////
// 
//	C interface function for using CUDA kernel : vectorAddKernel 
//
extern "C" bool swarmSearchWithCuda(int *x, int *y, long long* result, int arraySize, int mapSize)
{
	int *dev_x = 0;
	int *dev_y = 0;
	long long *dev_result = 0;

	int numberOfBlocks;			// number of thread blocks required in the grid
	int numMoves = 200;			// number of moves to be executed for each thread
	int maxDistPerMove = 50;	// maximum distance in x or y plane thread is allowed to move

	cudaError_t cudaStatus;
	curandState *devStates;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		goto Error;
	}

	//allocate buffers on device
	cudaStatus = cudaMalloc((void**)&dev_x, arraySize * sizeof(int));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}
	cudaStatus = cudaMalloc((void**)&dev_y, arraySize * sizeof(int));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}
	cudaStatus = cudaMalloc((void**)&dev_result, arraySize * sizeof(long long));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMalloc failed!");
		goto Error;
	}
	
	//copy inputs from host to device
	cudaStatus = cudaMemcpy(dev_x, x, arraySize * sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}
	cudaStatus = cudaMemcpy(dev_y, y, arraySize * sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}
	cudaStatus = cudaMemcpy(dev_result, result, arraySize * sizeof(long long), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	//launch kernal
	numberOfBlocks = (arraySize + blockSize - 1) / blockSize;
	cudaMalloc((void **)&devStates, arraySize * numberOfBlocks * sizeof(curandState));
	swarmSearchKernal << <numberOfBlocks, blockSize >> >(dev_result, dev_x, dev_y, arraySize, numMoves, maxDistPerMove, mapSize, devStates);

	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "swarmSearchKernal launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}

	// cudaDeviceSynchronize waits for the kernel to finish, and returns
	// any errors encountered during the launch.
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
		goto Error;
	}

	//copy memory from device to host
	cudaStatus = cudaMemcpy(result, dev_result, arraySize * sizeof(long long), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	cudaStatus = cudaMemcpy(x, dev_x, arraySize * sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

	cudaStatus = cudaMemcpy(y, dev_y, arraySize * sizeof(int), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
		goto Error;
	}

Error:
	cudaFree(dev_x);
	cudaFree(dev_y);
	cudaFree(dev_result);

	if (cudaStatus != cudaSuccess) return false;

	return true;
}

extern "C" bool resetCUDADevice()
{
	cudaError_t cudaStatus = cudaDeviceReset();
    if (cudaStatus != cudaSuccess) 
	{
        fprintf(stderr, "cudaDeviceReset failed!");
        return false;
    }

	return true;
}