//
//	main.cpp
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <math.h>
#include <assert.h>
#include <time.h>		// for clock_t and clock function
#include <Windows.h>	// for LARGE_INTEGER and QueryPerformanceFrequency function
#include "evalFunctions.h"

#include "timers.h"

#define numMovesPerThread 1000
#define maxDistancePerMove 50

const int arraySize =	100000;
const int mapSize	=	6000000;
int *x;
int *y;
long long *result;

int *gpuX, *gpuY;
long long *gpuResult;

int globalMinX;
int globalMinY;
long long globalMinResult;

extern "C" bool vectorAddWithCuda(float *c, float *a, float *b, unsigned int size);
extern "C" bool resetCUDADevice();
extern "C" bool swarmSearchWithCuda(int *x, int *y, long long *result, int arraySize, int mapSize);

long long rosen(int x, int y);
long long sphere(int x, int y);
long long booths(int x, int y);
long long evalFunc(int x, int y);
void swarmSearchWithCPU();

void swarmSearchWithCPU()
{
	int minX = x[0];
	int minY = y[0];
	long long minResult = result[0];
	int currDistPerMove = maxDistancePerMove;
	for (int i = 0; i < arraySize; i++)
	{
		currDistPerMove = maxDistancePerMove;	//reset currDistPerMove for every thread
		//move around starting point, looking for lower value
		for (int j = 0; j < numMovesPerThread; j++)
		{
			int xDir = rand() % 2 == 0 ? -1 : 1;
			int yDir = rand() % 2 == 0 ? -1 : 1;

			int xOffset = xDir * rand() % currDistPerMove;
			int yOffset = yDir * rand() % currDistPerMove;

			int newX;
			int newY;

			if (x[i] + xOffset < 0)
			{
				newX = mapSize + (x[i] + xOffset);
			}
			else
			{
				newX = (x[i] + xOffset) % mapSize;
			}

			if (y[i] + yOffset < 0)
			{
				newY = mapSize + (y[i] + yOffset);
			}
			else
			{
				newY = (y[i] + yOffset) % mapSize;
			}

			long long minResult = evalFunc(newX, newY);
			if (minResult < result[i])
			{
				x[i] = newX;
				y[i] = newY;
				result[i] = minResult;
				if(currDistPerMove > 0) currDistPerMove--;
			}
		}
		//set minimum to minimum found from thread
		if (minResult > result[i])
		{
			minX = x[i];
			minY = y[i];
			minResult = result[i];
		}
	}
	globalMinX = minX;
	globalMinY = minY;
	globalMinResult = minResult;
}

//========================================================================
//
//  main function
//
//	Compute vector add
//		c = a + b
//  on CPU and CUDA and compare their exection times and results.
//  
//========================================================================

// 33553920 = 512 * 65535 (using 512 threads block size; one vector sum per thread). 
// 65535 (2^16-1) is the maximum x-dim size allowed in SM 2.x or lower
// 2,147,483,647 (2^31-1) is the maximum x-dim size allowed in SM 3.0 or higher

int main()
{  
	time_t t;
	srand((unsigned)time(&t));
	x = (int*)malloc(arraySize * sizeof(int)); assert(x);
	y = (int*)malloc(arraySize * sizeof(int)); assert(y);
	result = (long long*)malloc(arraySize * sizeof(long long)); assert(result);

	gpuX = (int*)malloc(arraySize * sizeof(int)); assert(gpuX);
	gpuY = (int*)malloc(arraySize * sizeof(int)); assert(gpuY);
	gpuResult = (long long*)malloc(arraySize * sizeof(long long)); assert(gpuResult);
	double elapseTime;

	//init the arrays
	for (int i = 0; i < arraySize; i++)
	{
		x[i] = rand() % mapSize;
		y[i] = rand() % mapSize;
		result[i] = evalFunc(x[i], y[i]);
	}
	memcpy(gpuX, x, sizeof(x));
	memcpy(gpuY, y, sizeof(y));
	memcpy(gpuResult, result, sizeof(result));

	//printf("threads[0]: (%I64d, %I64d) = %I64d\n", threads[0].x, threads[0].y, threads[0].result);

	StartCounter();
	swarmSearchWithCPU();
	elapseTime = GetCounter();
	printf("CPU time: %.3f seconds\n", elapseTime);
	printf("CPU minimum Found: (%d, %d) = %I64d\n", globalMinX, globalMinY, globalMinResult);

	StartCounter();
	bool success = swarmSearchWithCuda(x, y, result, arraySize, mapSize);
	elapseTime = GetCounter();
	int minX = x[0];
	int minY = y[0];
	long long minResult = result[0];
	for (int i = 0; i < arraySize; i++)
	{
		if (result[i] < minResult)
		{
			minX = x[i];
			minY = y[i];
			minResult = result[i];
		}
	}
	printf("GPU time: %f seconds\n", elapseTime);
	printf("GPU minimum Found: (%d, %d) = %I64d\n", minX, minY, minResult);

    if (!success) 
	{
		fprintf(stderr, "addWithCuda failed!");
		free(x);
		free(y);
		free(result);
        return 1;
    }

    // cudaDeviceReset must be called before exiting in order for profiling and
    // tracing tools such as Nsight and Visual Profiler to show complete traces.
	resetCUDADevice();

	free(x);
	free(y);
	free(result);

	char ch;
	scanf_s("%c", &ch);

    return 0;
}